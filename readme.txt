=== Endeavr Dashboard ===
Contributors: jLOFT / Endeavr
Donate link: http://endeavr/wordpress/plugins/endeavr-dashboard/
Tags: admin,dashboard
Requires at least: 3.4
Tested up to: 3.5.1
Stable tag: 3.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==

This is a WordPress Dashboard plugin that works in tandem with the MP6 plugin ( http://wordpress.org/extend/plugins/mp6/ ). MP6 attempts to simplify and flatten the admin user interface.

The Endeavr Dashboard builds on top of MP6's framework to enhance the customer experience for Endeavr products.

== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Upload the `endvr-dashboard` folder to the `/wp-content/plugins/` directory or just use the built in WordPress plugin upload tool.
2. Activate the plugin through the 'Plugins' menu in WordPress
3. That's it! Have fun.

== Changelog ==

= 1.0 =
* First release