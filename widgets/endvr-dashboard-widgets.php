<?php

// Remove Some Default Dashboard Widgets
// @source: http://www.wprecipes.com/how-to-programatically-remove-wordpress-dashboard-widgets

function endvr_remove_dashboard_widgets() {
	global $wp_meta_boxes;

	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);

}
add_action('wp_dashboard_setup', 'endvr_remove_dashboard_widgets' );


// Add Custom Post Types to the Right Now Dashboard Widget
// @source: http://wpsnipp.com/index.php/functions-php/include-custom-post-types-in-right-now-admin-dashboard-widget/
function wph_right_now_content_table_end() {
 $args = array(
  'public' => true ,
  '_builtin' => false
 );
 $output = 'object';
 $operator = 'and';
 $post_types = get_post_types( $args , $output , $operator );
 foreach( $post_types as $post_type ) {
  $num_posts = wp_count_posts( $post_type->name );
  $num = number_format_i18n( $num_posts->publish );
  $text = _n( $post_type->labels->singular_name, $post_type->labels->name , intval( $num_posts->publish ) );
  if ( current_user_can( 'edit_posts' ) ) {
   $num = "<a href='edit.php?post_type=$post_type->name'>$num</a>";
   $text = "<a href='edit.php?post_type=$post_type->name'>$text</a>";
  }
  echo '<tr><td class="first b b-' . $post_type->name . '">' . $num . '</td>';
  echo '<td class="t ' . $post_type->name . '">' . $text . '</td></tr>';
 }
 $taxonomies = get_taxonomies( $args , $output , $operator );
 foreach( $taxonomies as $taxonomy ) {
  $num_terms  = wp_count_terms( $taxonomy->name );
  $num = number_format_i18n( $num_terms );
  $text = _n( $taxonomy->labels->singular_name, $taxonomy->labels->name , intval( $num_terms ));
  if ( current_user_can( 'manage_categories' ) ) {
   $num = "<a href='edit-tags.php?taxonomy=$taxonomy->name'>$num</a>";
   $text = "<a href='edit-tags.php?taxonomy=$taxonomy->name'>$text</a>";
  }
  echo '<tr><td class="first b b-' . $taxonomy->name . '">' . $num . '</td>';
  echo '<td class="t ' . $taxonomy->name . '">' . $text . '</td></tr>';
 }
}
add_action( 'right_now_content_table_end' , 'wph_right_now_content_table_end' );


// Add Dashboard Widgets
// @source: http://wpsnipp.com/index.php/functions-php/create-dashboard-widget/
// @source: http://wordpress.stackexchange.com/questions/4690/how-to-position-custom-dashboard-widgets-on-side-column

function endvr_dashboard_widget_one() { ?>
<div style="overflow:hidden;">
	<div style="width:60%; margin-right:10%; float:left;">
		<h4>Jason Loftis</h4>
		<ul>
			<li class="ss-phone"> &nbsp;619.365.5638</li>
			<li class="ss-mail"><a href="mailto:support@endeavr.com" title="Email Endeavr for Support"> &nbsp;support@endeavr.com</a></li>
			<li class="ss-desktop"><a href="http://endeavr.com" title="Endeavr.com" target="_blank"> &nbsp;www.endeavr.com</a></li>
		</ul>
	</div>
	<div style="width:30%; float:right">
		<img style="float:right" src="<?php echo ENDVR_DASHBOARD_URL . 'images/endeavr.png'; ?>" width="150" height="auto">
	</div>
</div>
<?php
}

add_action( 'wp_dashboard_setup', 'endvr_dashboard_setup_function' );
function endvr_dashboard_setup_function() {
    add_meta_box( 'endvr_dashboard_widget_one', 'Endeavr Customer Support', 'endvr_dashboard_widget_one', 'dashboard', 'normal', 'high' );
}

// Customize the text in the WP Admin footer
// http://wpsnipp.com/index.php/functions-php/custom-admin-footer/
function endvr_admin_footer() {
        echo 'Web Design + Development by ENDEAVR &#40;<a href="http://endeavr.com" title="ENDEAVR MEDIA - Mobilize Your Mission" target="_blank">www.endeavr.com</a>&#41;';
}
add_filter('admin_footer_text', 'endvr_admin_footer');

?>